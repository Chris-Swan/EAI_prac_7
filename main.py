"""
Handles the main function in the program.
"""

from knn import KNN
import numpy as np
import plotly.plotly as py
import plotly.graph_objs as go

def main():
    """The main function."""
    question1()


def question1():
    """The question 1 for the assignment."""
    data = get_file_data('Datasets/irisData/trainData.data')
    labels = get_file_data('Datasets/irisData/trainLabels.data')
    test_data = get_file_data('Datasets/irisData/testData.data')
    test_labels = get_file_data('Datasets/irisData/testLabels.data')
    test_classifications = []
    neighbours = KNN(data, labels)
    k = 1
    for test in test_data:
        test_classifications.append(neighbours.classify(k, test))
    make_pretty_graph(data, labels, test_data, test_classifications, "iris-classification-k1")
    errors_old = get_error(test_classifications, test_labels)
    print("Errors at k=%d are %d" % k, errors_old)
    stop = False
    while stop != True:
        k += 1
        name = 'iris-classification-k%d' % k
        test_classifications = []
        for test in test_data:
            test_classifications.append(neighbours.classify(k, test))
        make_pretty_graph(data, labels, test_data, test_classifications, name)
        errors = get_error(test_classifications, test_labels)
        stop = should_stop(errors_old, errors)
        errors_old = errors
        print("Errors at k=%d are %d" % k, errors_old)


def should_stop(errors_old, errors):
    """Checks if it should stop incrementing k."""
    if errors == 0:
        return True
    if errors > errors_old:
        return True
    else:
        return False


def get_error(test_classifications, test_labels):
    """Gets the errors in the Classification"""
    errors = 0
    for index in range(len(test_classifications)):
        if test_classifications[index] != test_labels[index]:
            errors += 1
    return errors


def make_pretty_graph(data, labels, test_data, test_classifications, name):
    """Makes a pretty graph with the data."""
    data_1 = []
    data_2 = []
    data_3 = []
    for index in range(len(data)):
        if labels[index] == 1:
            data_1.append(data[index])
        elif labels[index] == 2:
            data_2.append(data[index])
        elif labels[index] == 3:
            data_3.append(data[index])

    test_data_1 = []
    test_data_2 = []
    test_data_3 = []
    for index in range(len(test_data)):
        if test_classifications[index] == 1:
            test_data_1.append(test_data[index])
        elif test_classifications[index] == 2:
            test_data_2.append(test_data[index])
        elif test_classifications[index] == 3:
            test_data_3.append(test_data[index])

    x_1 = np.array(data_1)[:, 0]
    y_1 = np.array(data_1)[:, 1]
    z_1 = np.array(data_1)[:, 2]
    w_1 = np.array(data_1)[:, 3]

    x_2 = np.array(data_2)[:, 0]
    y_2 = np.array(data_2)[:, 1]
    z_2 = np.array(data_2)[:, 2]
    w_2 = np.array(data_2)[:, 3]

    x_3 = np.array(data_3)[:, 0]
    y_3 = np.array(data_3)[:, 1]
    z_3 = np.array(data_3)[:, 2]
    w_3 = np.array(data_3)[:, 3]

    x_t_1 = np.array(test_data_1)[:, 0]
    y_t_1 = np.array(test_data_1)[:, 1]
    z_t_1 = np.array(test_data_1)[:, 2]
    w_t_1 = np.array(test_data_1)[:, 3]

    x_t_2 = np.array(test_data_2)[:, 0]
    y_t_2 = np.array(test_data_2)[:, 1]
    z_t_2 = np.array(test_data_2)[:, 2]
    w_t_2 = np.array(test_data_2)[:, 3]

    x_t_3 = np.array(test_data_3)[:, 0]
    y_t_3 = np.array(test_data_3)[:, 1]
    z_t_3 = np.array(test_data_3)[:, 2]
    w_t_3 = np.array(test_data_3)[:, 3]

    trace_1 = go.Scatter3d(
        x=x_1,
        y=y_1,
        z=z_1,
        name='Iris Setosa',
        mode='markers',
        marker=dict(
            symbol='circle',
            size=12,
            color=w_1,
            colorscale='Viridis',
            opacity=0.8
        )
    )
    trace_2 = go.Scatter3d(
        x=x_2,
        y=y_2,
        z=z_2,
        name='Iris Versicolour',
        mode='markers',
        marker=dict(
            symbol='square',
            size=12,
            color=w_2,
            colorscale='Viridis',
            opacity=0.8
        )
    )
    trace_3 = go.Scatter3d(
        x=x_3,
        y=y_3,
        z=z_3,
        name='Iris Virginica',
        mode='markers',
        marker=dict(
            symbol='diamond',
            size=12,
            color=w_3,
            colorscale='Viridis',
            opacity=0.8
        )
    )
    trace_4 = go.Scatter3d(
        x=x_t_1,
        y=y_t_1,
        z=z_t_1,
        name='Iris Setosa Classification',
        mode='markers',
        marker=dict(
            symbol='circle-open',
            size=12,
            color=w_t_1,
            colorscale='Viridis',
            opacity=0.8
        )
    )
    trace_5 = go.Scatter3d(
        x=x_t_2,
        y=y_t_2,
        z=z_t_2,
        name='Iris Versicolour Classification',
        mode='markers',
        marker=dict(
            symbol='square-open',
            size=12,
            color=w_t_2,
            colorscale='Viridis',
            opacity=0.8
        )
    )
    trace_6 = go.Scatter3d(
        x=x_t_3,
        y=y_t_3,
        z=z_t_3,
        name='Iris Virginica Classification',
        mode='markers',
        marker=dict(
            symbol='diamond-open',
            size=12,
            color=w_t_3,
            colorscale='Viridis',
            opacity=0.8
        )
    )
    dataset = [trace_1, trace_2, trace_3, trace_4, trace_5, trace_6]
    layout = go.Layout(
        title=name,
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0
        )
    )
    fig = go.Figure(data=dataset, layout=layout)
    py.iplot(fig, filename=name)



def get_file_data(filename):
    """Gets the flower data."""
    fileobject = open(filename, 'r')
    line = fileobject.readline()
    inputs = []
    index = 0
    while line != "":
        if ',' in line:
            inputs.append([])
            for item in line.rstrip().split(','):
                inputs[index].append(float(item))
        else:
            inputs.append(float(line))
        line = fileobject.readline()
        index += 1
    return inputs




main()
