"""
The K nearest neighbours class.
"""

import math
import numpy as np
from collections import Counter


class KNN(object):
    """The k nearest neighbours class."""
    def __init__(self, data, labels):
        self._data = self._set_data(data, labels)

    def classify(self, k, sample):
        """Classifies a given sample based on its data."""
        arrtest = self._set_test_array(k)
        for item in self._data:
            distance = self._get_distance(sample, item)
            if distance < arrtest[-1][0]:
                arrtest[-1][0] = distance
                arrtest[-1][1] = item[-1]
                arrtest.sort(key=lambda x: x[0])
        count = Counter(np.array(arrtest)[:, 1])
        return count.most_common(1)[0][0]

    @staticmethod
    def _get_distance(sample, item):
        """Finds the euclidean distance between a sample and an item."""
        squared_distance = 0
        for index in range(len(item) - 1):
            squared_distance += math.pow(sample[index] - item[index], 2)
        return math.sqrt(squared_distance)

    @staticmethod
    def _set_test_array(k):
        """Sets up a test array with max values to be used."""
        arrtest = []
        for _ in range(k):
            arrtest.append([999999999, 0])
        return arrtest

    @staticmethod
    def _set_data(data, labels):
        """Structures the data in a manner usable by the program."""
        dataset = []
        for index in range(len(data)):
            dataset.append(data[index])
            dataset[-1].append(labels[index])
        return dataset
